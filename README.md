# PizzaStore frontend project

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
The backend part of the PizzaStore project, made with NodeJS, ExpressJS
The database used is Postgres.

# Rapport - Frontend
Rapport för backenden hittar du i README.md i projektet pizzastore-backend: https://github.com/j-development/pizzastore-backend
# Allmänt
Vi har skapat en hemsida och ett administratörssystem för en pizzeria.
"Hemsidan" har som huvudfunktion att kunna lägga en onlinebeställning.
Administratörssystemet har som huvudfunktion att kunna uppdatera menyn.
# Tillvägagångssätt
Vi började med att diskutera olika ideer. Därefter letade vi upp en referenshemsida och bestämde funktionerna tillsammans. 
Sedan delade vi in oss i ett backend och ett frontend team. Vi har kontinuerligt uppdaterat varandra, berättat vad vi har gjort och vad vi har behövt från den andra sidan.
Till viss del har vi också varit inne och bugfixat/uppdaterat funktioner över teamgränserna. 
# Struktur
    Index.js - Vår ingångspunkt
        Då index.js är reacts ingångspunkt och allt som renderas renderas härifrån så har vi valt att hantera sådant som rör alla views på hemsidan här. I detta fall har vi "aktiverat" routing och context i index.js. Detta görs ofta i App.js men vi har istället valt att App.js ska vara ingångspunkten till den publika delen av hemsidan.

    App.js - Vårt shell till den publika delen av pizzastore.
        I app.js har vi byggt upp vårt shell för den publika delen. Alltså lagt till de komponenter som ska vara återkommande på alla publika sidor.(Header & footer)
        Vi har även fortsatt bygga vårt routingsystem. Med hjälp av path='/*' (Alla odefinerade routes) navigerar vi tillbaks till Home.js vilket tar bort 404-problemet.

    Utils - Innehåller annan övrig kod, i detta fall koden som kopplar front-enden mot servern.
        Api.js - Alla anrop som pratar direkt med api:et.
            Vi har en del publika endpoints och en del "låsta". Vi har valt att då vi kontaktar publika endpoints gör vi det direkt ifrån api.js. I de fall vi vill nå de "låsta" endpointsen går vi först genom ett context som såklart i sin tur anropar api.js

    Views - Varje viewkomponent motsvarar en hel sida(url) på hemsidan.
        Menu.js
            Hämtar ut och mappar upp alla pizzor från servern, renderar dem i grupper och sedan varje pizza med tillhörande counter.
        Cart.js
            Visar alla pizzor man lagt i kundkorgen. Alltså de pizzor som ligger i contextet cart.
        Login.js
            Första steget för att nå admindelen. Om man redan är inloggad navigeras man direkt vidare till admin.js.
            Skriv i ett password och lösenord. Efter submit körs en auth-funktion med servern. Funkar allt kommer usercontext uppdateras och med hjälp av en useEffect som körs vid varje rendering navigeras vi till admin.js.
        Admin.js
            Mappar ut alla pizzor i en tabell. Klickar man på en pizza kan man ändra den. Genom en overlay som läggs på den nu valda pizzan kan vi inte återigen klicka på samma pizza. Detta för att ta bort buggar.
            Vi kan även ta bort och lägga till helt nya pizzor.
            
    Context - Är vår mapp som hanterar våra globala states. Själva statsen och tillhörande funktioner såsom reducer. 
        CartContext.js / CartReducer.js
            Håller information om vad som finns i kundkorgen. Istället för att använt oss av ett vanligt state har vi valt att använda reacts useReducer. Dispatch-funktionen tar emot ett statet innan uppdatering och en action. I vårt fall består en action av ett object där "type" är obligatoriskt. Type berättar vad vi ska göra med statet, typ add, clear och remove. En reducer är bra när man vill göra väldigt många olika saker med ett state. Med hjälp av dispatch får vi många olika funktioner men behöver endast skicka med en funktion ut till de olika kompontenterna.
        UserContext.js
            Håller reda på om en användare är inloggad genom ett userState. Om en användare är inloggad fixar userContext så att varje anrop innehåller de nödvändiga tokensen som behövs för att prata med servern. Detta utan att vi behöver göra något speciellt i komponenterna utöver att kalla på funktionerna. Typ savePizza.
    Components - Små återanvändningsbara komponenter. Med hjälp av dessa bygger vi upp viewsen.
        Counter.js
            En funktion som återkommer både i menu.js och cart. Den är direkt kopplad mot cartcontext och ökar|lägger till/minskar|tar bort pizzor. För att göra den ännu mer återänvändingsbar skulle man kunna ta emot en increase och en decrease funktion som props.
    Style | Media - CSS-filer och media såsom bilder och videos. 


# Övrigt
**Browserrouter istället för memory, och hash.** <br />
    Memory-router frikopplar sin routing helt från URL-en. Detta är perfekt då man t.ex. bygger mot mobil och framförallt kanske bygger en PWA och vet att användaren inte kommer ha tillgång till ett URL-fält.
    Att vi valde browserrouter över hashrouter är för att React rekommenderar detta plus att vi ville ha möjligheten att nå en viss del av sidan direkt från URL:en på ett sätt som den gemene användaren är van vid.
    Med hashrouter används # i url och alla calls refererar till baseurl:en och sen navigeras man framåt på sidan med hjälp av det efter hash. Detta innebär att localhost/menu skulle ta oss till localhost endast.
    Vi har inte stött på det men när man levererar sidor från servern är det viktigt att man kopplar alla url:er på rätt sätt. Det är det problemet som är återkommande i forum när man pratar browserrouter.

**React Context över Redux och Recoil** <br />
    Vi valde reacts inbyggda system för global state management. Anledningarna är flera men nummer 1 är att hela vårt team var bekanta med context. Detta i sig är en viktig faktor för när man väljer/inte väljer bibliotek för att underlätta byggprocessen. 
    Annars tyckte att vi det var positivt att använda det inbyggda systemet för att minska build-storleken. Sett till tillexempel redux så finns det mycket funktionalitet i redux som sällan används och som vi inte har haft behov av i detta projekt. Att då använda redux ger oss en massa extra oanvänd onödig kod i builden.
    React context är också bra då man vill ge en viss del av sidan tillgång till vissa global states och andra inte. Då varje context hör hemma i sin egen fil kan man enkelt dela ut ett specifikt globalt state på en specifik plats.
    Om vi däremot skulle haft med komplex state-management som skulle nås av alla komponontenter hade Recoil.js kunna varit ett bra alternativ, enkelt & med lite boilerplatekod.

**Tailwind** <br />
    Vi testade att använda tailwindcss i Header.js. Efter att ha varit ganska anti CSS och component-frameworks då det många gånger har känts som att man blivit begränsad så var tailwind en glad överaskning. Du har precis samma frihet som i vanlig css men ett lättare sätt att skriva den. Men hjälp av tailwind kan man slippa tänka på namn för alla klasser vilket lätt blir en stor röra och som drar med sig en stor risk över att man råkar döpa flera till samma eller misslyckas med att targeta rätt div:ar eller liknande. Med tailwind kan man faktiskt få komponenterna att bli mer återanvändningsbara i andra projekt. Vid implementering i ett annat projekt behöver man inte tänka på om det nya projektet följers ens tidigare namnstruktur utan all information om kompononenten finns i komponenten. En bonus är att tailwindUI finns som är tailwind komponenter och att det är lätt att använda tailwind med t.ex. headlessUI som också levererar färdiga komponenter med all funktionalitet och accessibility included.

# Utveckling
**Ta bort knapp.** <br />
    För att göra det enklare för pizzabutikens affärer bör en knapp som tar bort alla pizzor implementeras. Just nu finns bara möjligheten att minska antalet pizzor steg för steg till 0. 

**Bättre design** <br />
    Framförallt behövs sidan göras responsiv. I vanlig ordning har vi inte kört mobile-first vilket innebär ett ganska stort merjobb för att mobilanpassa. För den gemene användaren så lär de flesta beställningar läggas från mobil. Detta gäller internet överlag, en stor del trafik kommer från mobil. Därför är det klokt att i framtiden(när man jobbar med publika hemsidor och inte system) att anamma mobile-first. 

**Hem och kontaktsida** <br /> 
    Dessa är än så länge helt tomma i brist på tid.

**Pushnotis/socketkoppling för nya ordrar** <br />
    Nästa steg i administratörsssystem är att koppla ordrarna i databasen till frontenden gärna med någon typ av notis/pushnotis. Ett möjligt tillvägagångssätt skulle vara att med hjälp av socket.io sätta upp en kanal mellan klienten och servern när man loggar in på administratörshemsidan. Därefter varje gång en ny order läggs till kan man skicka ett meddelande till klientet som i sin tur fetchar databasens ordrar.

**Snyggare alerts och confirmations** <br />
    I dagsläget använder vi browserns inbyggda alerts och confirm. Dessa fungerar men för att ge ett mer proffesionellt intryck bör vi skapa egendesignade kompontenter med samma funktionalitet.

**Bryta ut kod** <br />
    Framförallt admindelen behöver byggas mer funktionellt och modulärt. Som grund ska varje komponent i react göra "1 sak". Detta stämmer inte på admindelen. I dagsläget pågrund av bristande erfarenhet är det lättare att bygga funktionalitet för att sedan bryta ut kod till passande komponenter(vilket vi inte hunnit). Förhoppningsvis, och med mer träning, så blir det lättare att redan innan kunna planera för och se vilka komponenter som bör byggas och var uppdelningen bör göras.

**validateUser** <br />
    validateUser funktionen behöver uppdateras. Vi har en validatefunktion i backenden som kan testa så att den inloggade personen har aktiva tokens. Men just nu blir man vidarenavigerad till admin-delen så länge någon är inloggad. Däremot kan man inte göra några updates/deletes eller creates genom att fejka tokens då en verifiering sker vid varje sådant call.


