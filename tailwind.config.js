/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    fontFamily: {
      serif: ['cursive', 'serif'],
    },
    extend: {
      keyframes: {
        popout: {
          '0%': { transform: 'scale(0)' },
          '50%': { transform: 'scale(1.1)' },
          '100': { transform: 'scale(1)' }
        }
      },
      animation: {
        popout: 'popout 1s 1 forwards ease-in-out',
      }
    },
  },
  plugins: [],
}
