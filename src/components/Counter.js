import { useContext } from "react";
import { CartContext } from "../contexts/CartContext";
import '../style/counter.css';

const Counter = ({pizza}) => { 
    const {dispatch, getPizzaCount} = useContext(CartContext);

    const getCount = () => {
        return getPizzaCount(pizza.id);
    }

    const handleDecrease = () => {
        dispatch({type: 'REMOVE', pizza: pizza})
    }

    const handleIncrease = () => {
        dispatch({type: 'ADD', pizza: pizza})
    }

    return (
        <div className="counter">
            <p className="d" onClick={handleDecrease}>-</p>
            <p className="q">{getCount()}</p>
            <p className="i" onClick={handleIncrease}>+</p>
        </div>
    );
}
 
export default Counter;