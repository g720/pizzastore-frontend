import '../style/footer.css'
import { useNavigate } from 'react-router-dom';

const Footer = () => {
    let navigate = useNavigate();

    return (
        <div className='footer'>
            <div>
                <p>Besök oss på:</p>
                <p>Storgatan 9</p>
                <p>362 55 Osby</p>
                <p>Öppet: 11-21</p>
            </div>
            <div>
                <p>PizzaStore.se</p>
                <p>0123-4567</p>
                <p>info@pizzastore.se</p>
            </div>
            <div>
                <p>Developed by StudieGrupp 6</p>
                <p className='adminBtn' onClick={() => navigate('/admin')}>Admin Dashboard</p>
            </div>
        </div>
    );
}
 
export default Footer