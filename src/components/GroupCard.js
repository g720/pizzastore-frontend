import { useEffect, useState } from "react";
import MenuModal from "./MenuModal";
import '../style/groupCard.css'

const GroupCard = ({pizzaList, pizzaGroup}) => {
    const [modalOpen, setModalOpen] = useState(false);
    const [pizzaListByGroup, setPizzaListByGroup] = useState([]);

    useEffect(() => {
        setPizzaListByGroup(() => {
            return pizzaList.filter(pizza => pizza.pizzagroup_id === pizzaGroup.id)
        });
    }, [pizzaList, pizzaGroup])

    const openModal = () => {
        setModalOpen(true);
    }

    const closeModal = () => {
        setModalOpen(false);
    }

    return (
        <div onClick={openModal} className="groupCard">
            <p className="title">{pizzaGroup.name}</p>
            <img className="img" src="https://gray-wwny-prod.cdn.arcpublishing.com/resizer/l_ZlW86PPJ0CTOQ6SDMPW4Si8KU=/1200x675/smart/filters:quality(85)/cloudfront-us-east-1.images.arcpublishing.com/gray/YMFAE4MTJZD5RIN2ZAZII2RRII.jpg" alt="pizzapic"></img>
            {modalOpen ? <MenuModal closeModal={closeModal} pizzaList={pizzaListByGroup}/> : ''}
        </div>
    );
}
 
export default GroupCard;