import { useNavigate} from "react-router-dom";
import { useContext } from 'react';
import { CartContext } from '../contexts/CartContext';

const Header = () => {
    const { getTotalCount } = useContext(CartContext);
    const navigate = useNavigate();

    return (
        <>
        <header className='w-full bg-black'>
            <div className='w-full flex justify-evenly p-2'>
                <p className='text-white text-2xl items-center font-serif'>PizzaStore.se</p>
                <p className='text-white items-center font-serif hidden lg:flex'>Öppet 11:00-21:00</p>
                <p className='text-white items-center font-serif hidden lg:flex'>Beställ online eller på telefon: 0123-4567</p>
                <p className='text-white flex items-center font-serif hidden sm:flex lg:hidden'>11:00-21:00 | 0123-4567</p>
            </div>
        </header>
            <nav className='w-full border-b-2 border-red-800 border-solid flex p-2 justify-between items-center'>
                <div className='flex flex-col sm:gap-4 sm:flex-row'>
                    <p className='text-lg font-bold cursor-pointer sm:ml-3' onClick={() => navigate('/')}>Hem</p>
                    <p className='text-lg font-bold cursor-pointer' onClick={() => navigate('/menu')}>Meny</p>
                    <p className='text-lg font-bold cursor-pointer' onClick={() => navigate('/contact')}>Kontakt</p>
                </div>
                <div className='cursor-pointer font-bold mr-5 relative' onClick={() => navigate('/cart')}>Kundvagn
                    {getTotalCount() == 0 ? '' : <p className='animate-popout absolute top-[-5px] right-[-4px] bg-red-600 rounded-[50%] py-[3px] px-[4px] flex justify-center items-center text-[10px] [aspect-ratio:1]'>{getTotalCount()}</p>}
                </div>
            </nav>
        </>
    );
}
 
export default Header