import Counter from "./Counter";
import '../style/menuModal.css';

const MenuModal = ({pizzaList, closeModal}) => {
    
    const handleClick = (e) => {
        e.stopPropagation();
        closeModal();
    }
    
    return (
        <div className="menuModal">
            <p className="closebtn" onClick={handleClick}>X</p>
            {
                pizzaList.map((pizza, index) => {
                    return <div className="menuItem" key={pizza.id + index}>
                            <p className="pizzaName">{pizza.name}</p>
                            <p className="pizzaPrice">{pizza.price}kr</p>
                        <Counter pizza={pizza}/>
                    </div>
                })
            }
        </div>
    );
}
 
export default MenuModal;