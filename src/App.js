import { Routes, Route, Navigate } from 'react-router-dom';
import Footer from './components/Footer';
import Header from './components/Header';
import Cart from './views/Cart';
import Contact from './views/Contact';
import Home from './views/Home';
import Menu from './views/Menu';
import './App.css';

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/contact" element={<Contact />} />
        <Route path="/menu" element={<Menu />} />
        <Route path="/cart" element={<Cart />} />
        <Route path='/*' element={<Navigate replace to='/' />} />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
