const API_URL = 'http://localhost:3050/';

function url(key) {
  return API_URL + key;
}

export async function apiGetPizzaList() {
  let response = await fetch(url('pizza'));
  return response.json();
}

export async function apiGetDistinctGroups() {
  let response = await fetch(url('pizza/groups'));
  return response.json();
}

export async function apiPostPizza(name, price, pizzagroup, token) {
  let response = await fetch(url('pizza'), {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + token,
    },
    body: JSON.stringify({
      name,
      price,
      pizzagroup,
    }),
  });
  if (response.status !== 200) {
    return 'INVALID';
  } else {
    return response.json();
  }
}

export async function apiUpdatePizza(id, name, price, pizzagroup, token) {
  let response = await fetch(url('pizza'), {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + token,
    },
    body: JSON.stringify({
      id,
      name,
      price,
      pizzagroup,
    }),
  });
  if (response.status !== 200) {
    return 'INVALID';
  } else {
    return response.json();
  }
}

export async function apiDeletePizza(pizzaId, token) {
  let response = await fetch(url(`pizza/${pizzaId}`), {
    method: 'DELETE',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  });
}

export async function apiLogIn(username, password) {
  let response = await fetch(url('auth/login'), {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      username: username,
      password: password,
    }),
  });
  if (response.status !== 200) {
    console.log(await response.json());
    return 'INVALID';
  } else {
    return response.json();
  }
}

export async function apiCreateOrder(orderObj) {
  console.log(orderObj);
  let response = await fetch(url('pizza/order'), {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(orderObj),
  });
  if (response.status !== 200) {
    console.log(await response.json());
    return 'INVALID';
  } else {
    return response.json();
  }
}
