import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import Login from './views/Login';
import Admin from './views/Admin';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import CartContextProvider from './contexts/CartContext';
import UserContextProvider from './contexts/UserContext';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <CartContextProvider>
    <UserContextProvider>
      <BrowserRouter>
        <React.StrictMode>
          <Routes>
            {/* Med hjälp av stjärna/wildcard så säger vi att app ska laddas tillsammans med nästkommande route */}
            <Route path="/*" element={<App />} />
            <Route path="/login" element={<Login />} />
            <Route path="/admin" element={<Admin />} />
          </Routes>
        </React.StrictMode>
      </BrowserRouter>
    </UserContextProvider>
  </CartContextProvider>
);
