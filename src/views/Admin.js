import { useEffect, useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { UserContext } from '../contexts/UserContext';
import { apiGetPizzaList, apiGetDistinctGroups } from "../utils/api.js"
import '../style/admin.css';

const Admin = () => {
    const { validateUser, clearUser, savePizza, deletePizza } = useContext(UserContext);
    const [currentPizza, setCurrentPizza] = useState(null);
    const [pizzaList, setPizzaList] = useState([]);  
    const [groups, setGroups] = useState([]);

    const [inputName, setInputName] = useState('');
    const [inputGroup, setInputGroup] = useState(1);
    const [inputPrice, setInputPrice] = useState(0);

    let navigate = useNavigate();

    useEffect(() => {
        if(currentPizza){
            setInputGroup(currentPizza.pizzagroup_id);
            setInputName(currentPizza.name);
            setInputPrice(currentPizza.price);
        }else{
            setInputGroup(1);
            setInputName('');
            setInputPrice(0);
        }
    }, [currentPizza])

    const updateSelect = (evt) => {
        setInputGroup(evt.target.value);
    }
    const updateName = (evt) => {
        setInputName(evt.target.value);
    }
    const updatePrice = (evt) => {
        setInputPrice(evt.target.value);
    }
    const clearInputs = () => {
        setInputName('');
        setInputGroup(1);
        setInputPrice(0);
        setCurrentPizza(null);
    }
    const saveClick = () => {
        let confirmation;
        if(currentPizza){
            confirmation = window.confirm(`Är du säker på att du vill uppdatera den valda pizzan:\n
            Id: ${currentPizza.id}
            Namn: ${inputName}
            Grupp: ${getGroupName(inputGroup)}
            Pris: ${inputPrice}
            `)
        }else{
            confirmation = window.confirm(`Är du säker på att du vill skapa en ny pizza:\n
            Id: Ny pizza
            Namn: ${inputName}
            Grupp: ${getGroupName(inputGroup)}
            Pris: ${inputPrice}
            `)
        }
        if(confirmation){
            if(currentPizza){
                savePizza({
                    id: currentPizza.id,
                    name: inputName,
                    pizzagroup: inputGroup,
                    price: inputPrice,
                }).then(() => {
                    fetchPizzas();
                });
            }else{
                savePizza({
                    id: null,
                    name: inputName,
                    pizzagroup: inputGroup,
                    price: inputPrice,
                }).then(() => {
                    fetchPizzas();
                })
            }
        }
        clearInputs();
    }

    const deleteClick = () => {
        if(currentPizza){
            let confirmation = window.confirm(`Är du säker på att du vill radera följande pizza:\n
            Id: ${currentPizza.id}
            Namn: ${currentPizza.name}
            Grupp: ${getGroupName(currentPizza.pizzagroup)}
            Pris: ${currentPizza.price}
            `)
            if(confirmation){
                deletePizza(currentPizza).then(() => {
                    clearInputs();
                    fetchPizzas();
                });
            }
        }
    }

    useEffect(() => {
        if(!validateUser(true)){
            navigate('/login')
        }
        fetchPizzas();
        fetchGroups();
    }, [])

    const fetchPizzas = () => {
        apiGetPizzaList().then(setPizzaList);
    }

    const fetchGroups = () => {
        apiGetDistinctGroups().then(setGroups);
    }

    const logout = () => {
        clearUser();
        navigate('/login')
    }

    const handleItemClick = (pizza) => {
        setCurrentPizza(pizza)
    }

    const getGroupName = (id) => {
        return groups.filter(group => group.id === id).map(group => group.name)[0];
    }

    return (
        <div className='admin'>
            <div className='adminHeader'>Admin dashboard for PizzaStore.se</div>
            <p style={{marginLeft: "1rem"}}>Select pizza to change:</p>
            <div className='editContainer'>
                <table className='left'>
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Namn</th>
                        <th>Grupp</th>
                        <th>Pris</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        pizzaList.map((pizza, index) => {
                            return <tr className="pizzaItem" onClick={() => handleItemClick(pizza)} key={pizza.id + index + (Math.random()*1000)}>
                                <td>{pizza.id}</td>
                                <td>{pizza.name}</td>
                                <td>{getGroupName(pizza.pizzagroup_id)}</td>
                                <td>{pizza.price}kr</td>
                                {currentPizza && pizza.id === currentPizza.id && <td onClick={(e) => { e.stopPropagation() }} className='selectedOverlay'></td>}
                            </tr>
                        })
                    }
                    </tbody>
                </table>
                <div className='right'>
                    <div className='btn createNewBtn' onClick={clearInputs}>Skapa ny pizza</div>
                    <div className='inputBox'>
                        <label>Namn:</label>
                        <input type={"text"} onChange={updateName} value={inputName}></input>
                    </div>
                    <div className='inputBox'>
                        <label>Pris:</label>
                        <input type={"number"} onChange={updatePrice} value={inputPrice}></input>
                    </div>
                    <select name="groups" id="groupselect" value={inputGroup} onChange={updateSelect}>
                        {
                            groups.map((group, index) => {
                                return <option value={group.id} key={group.name + index + group.id}>{group.name}</option>
                            })
                        }
                    </select>
                    <div className='btn saveBtn' onClick={saveClick}>Spara</div>
                    <div className='btn cancelBtn' onClick={clearInputs}>Avbryt</div>
                    <div className='btn deleteBtn' onClick={deleteClick}>Radera den valda pizzan</div>
                    <div className='btn logoutBtn' onClick={logout}>Logga ut</div>
                    <div className='btn goToBtn' onClick={() => {navigate('/')}}>Besök PizzaStore.se</div>
                </div>
            </div>
        </div>
    );
}
 
export default Admin