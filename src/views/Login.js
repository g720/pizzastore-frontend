import { useContext, useEffect, useState } from 'react';
import { UserContext } from '../contexts/UserContext';
import { useNavigate } from 'react-router-dom';
import '../style/login.css';

const Login = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const { logIn, validateUser } = useContext(UserContext);
  let navigate = useNavigate();

  useEffect(() => {
    if (validateUser(true)) {
      navigate('/admin');
    }
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    logIn(username, password);
  };

  const handleChange = (setValue, value) => {
    setValue(value);
  };

  return (
    <div className="outer">
      <div className="login-container">
        <form>
          <label>
            Enter username
            <input
              type="text"
              value={username}
              onChange={(e) => {
                handleChange(setUsername, e.target.value);
              }}
            />
          </label>

          <label>
            Enter password
            <input
              type="text"
              value={password}
              onChange={(e) => {
                handleChange(setPassword, e.target.value);
              }}
            />
          </label>
          <button className="button-container" onClick={handleSubmit}>
            Login
          </button>
        </form>
      </div>
    </div>
  );
};

export default Login;
