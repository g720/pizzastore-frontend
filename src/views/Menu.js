import { useEffect, useState } from "react";
import GroupCard from "../components/GroupCard";
import '../style/menu.css';
import { apiGetPizzaList, apiGetDistinctGroups } from "../utils/api.js"

const Menu = () => {
    //Jag använder inte ett globalstate här då vi pizzalistan endast ska röra sig nedåt två steg. Inga syskon, föräldrar eller någon överdriven propdrilling.
    const [pizzaList, setPizzaList] = useState([]);
    const [groups, setGroups] = useState([]);

    useEffect(() => {
        apiGetPizzaList().then(setPizzaList);
        apiGetDistinctGroups().then(setGroups);
    }, [])

    return (
        <div className="menuView">
            <div className="img-container">
                <h1>Meny</h1>
            </div>
            <div className="groupCard-container">
                {
                    groups.map((pizzaGroup, index) => {
                        return <GroupCard pizzaList={pizzaList} pizzaGroup={pizzaGroup} key={pizzaGroup.id + index} />
                    })
                }
            </div>
        </div>
    );
}
 
export default Menu;