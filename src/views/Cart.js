import { useContext, useState } from 'react';
import Counter from '../components/Counter';
import { CartContext } from '../contexts/CartContext';
import '../style/cart.css';
import { apiCreateOrder } from '../utils/api';

const Cart = () => {
  const { cart, getTotalPrice, getPizzaCount, dispatch} = useContext(CartContext);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [freeText, setFreeText] = useState('');

  const handleSubmit = (event) => {
    event.preventDefault();

    const orderObj = {
      customer_name: name,
      itemList: [],
    };

    cart.forEach((element) => {
      const pizzaObj = {
        pizza_id: element.id,
        quantity: element.quantity,
      };
      orderObj.itemList.push(pizzaObj);
    });

    apiCreateOrder(orderObj);
    dispatch({type: 'CLEAR', pizza: ''});
    setName('');
    setEmail('');
    setFreeText('');
    alert("Tack för din beställning. Klar om 15min");
  };

  return (
    <form className="cart" onSubmit={handleSubmit}>
      <div className="cartItems">
        {cart.map((pizza, index) => {
          return (
            <div className="cartItem" key={index}>
              <div className="cartItemInfo">
                <p style={{ fontFamily: 'cursive', fontSize: '130%' }}>
                  {pizza.name}
                </p>
                <div className="priceInfo">
                  <p>{pizza.price}kr</p>
                  <p>x {getPizzaCount(pizza.id)}</p>
                  <p>= {getPizzaCount(pizza.id) * pizza.price}kr</p>
                </div>
              </div>
              <Counter pizza={pizza} />
            </div>
          );
        })}
        <p className="totalPriceInfo">{getTotalPrice()}kr</p>
      </div>
      <div className="right">
        <div className="extras">
          <textarea placeholder="Skriv extra information gällande din beställning här!" value={freeText} onChange={(e) => {
            setFreeText(e.target.value);
          }}></textarea>
        </div>
        <div className="pay">
          <label>Namn:</label>
          <input
            id="name"
            required
            type={'text'}
            value={name}
            onChange={(e) => {
              setName(e.target.value);
            }}
          ></input>
          <label>Email:</label>
          <input id="email" required type={'email'} value={email} onChange={(e) => {
            setEmail(e.target.value);
          }}></input>
          <button id="submitBtn" type="submit">
            Lägg beställning
          </button>
        </div>
      </div>
    </form>
  );
};

export default Cart;
