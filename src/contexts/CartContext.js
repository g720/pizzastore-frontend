import React, {createContext, useState, useEffect, useReducer} from 'react';
import { CartReducer } from './CartReducer';

export const CartContext = createContext();

//Det är lite mer boilerplatekod i context gentemote recoil. Men då vi bara ska spara ett enda state föredrar vi att använda det "Inbyggda" statehanteringsbiblioteket
const CartContextProvider = (props) => {
    const [cart, dispatch] = useReducer(CartReducer, []);

    const getPizzaCount = (pizzaId) => {
        let cartItem = cart.find(item => item.id === pizzaId);
            if (cartItem) {
                return cartItem.quantity;
            } else {
                return 0;
            }
    }

    const getTotalCount = () => {
        return cart.reduce((prev, current) => prev + current.quantity, 0)
    }

    const getTotalPrice = () => {
        return cart.reduce((prev, current) => prev + (current.quantity * current.price), 0)
    }

    return (
        <CartContext.Provider value={{cart, dispatch, getPizzaCount, getTotalPrice, getTotalCount}}>
        {props.children}
        </CartContext.Provider>
    );
}
 
export default CartContextProvider;