import React, {createContext, useState} from 'react';
import { apiLogIn, apiPostPizza, apiDeletePizza, apiUpdatePizza } from '../utils/api';

export const UserContext = createContext();

const UserContextProvider = (props) => {
    const [user, setUser] = useState(null);


    const logIn = (password, username) => {
        apiLogIn(password, username).then(data => {
            if(data === "INVALID"){
                clearUser();
            }else{
                setUser(data)
            }
        })
    }

    const validateUser = (booleanIn) => {
        if(!user){
            return false;
        }
        if(booleanIn){
            return true;
        }else{
            //Validate token thruu api.
            clearUser()
            return false;
        }
    }

    async function savePizza(pizzaObj){
        if(pizzaObj.id){
            return apiUpdatePizza(pizzaObj.id, pizzaObj.name, pizzaObj.price, pizzaObj.pizzagroup, user.access_token)
        }else{
            return apiPostPizza(pizzaObj.name, pizzaObj.price, pizzaObj.pizzagroup, user.access_token);
        }
    }

    async function deletePizza(pizzaObj){
        return apiDeletePizza(pizzaObj.id, user.access_token);
    }

    const clearUser = () => {
        setUser(null);
    }

    return (
        <UserContext.Provider value={{logIn, validateUser, clearUser, savePizza, deletePizza}}>
        {props.children}
        </UserContext.Provider>
    );
}
 
export default UserContextProvider;