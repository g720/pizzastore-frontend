export const CartReducer = (state, action) => {
    let found = false;
    let newState;
    let pizza = action.pizza;
    switch (action.type) {
        case "ADD":
            newState = state.map(cartItem => {
                if(cartItem.id === pizza.id){
                    cartItem.quantity = cartItem.quantity + 1
                    found = true;
                }
                return cartItem;
            });
            if(!found){
                newState.push({
                    ...action.pizza,
                    "quantity": 1
                })
            }
            return newState;
        case "REMOVE":
            newState = state.map(cartItem => {
                if(cartItem.id === pizza.id){
                    cartItem.quantity = cartItem.quantity - 1
                }
                return cartItem;
            })
            return newState.filter(cartItem => cartItem.quantity > 0);
            case "CLEAR":
                return [];
        default:
            return state;
    }
}